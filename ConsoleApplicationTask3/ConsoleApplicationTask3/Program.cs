﻿using System;

namespace ConsoleApplicationTask3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Create Polynomial --- new Polynomial(80, 20, 50, 90)");
            Console.WriteLine("Create Polynomial --- new Polynomial(5, 3, 54)");
            Polynomial p1 = new Polynomial(80, 20, 50, 90);
            Polynomial p2 = new Polynomial(5, 3, 54);
            Console.WriteLine(p1);
            Console.WriteLine(p2);
            Console.WriteLine();

            Console.WriteLine("Calculate the value of 3");
            Console.WriteLine((p1).Calculate(3));
            Console.WriteLine("Calculate the value of -3");
            Console.WriteLine((p1).Calculate(-3));
            Console.WriteLine();

            Console.WriteLine("Add two polynomial");
            var polSum = Polynomial.Summation(p1, p2);
            Console.WriteLine(polSum);
            Console.WriteLine();

            Console.WriteLine("Subtract two polynomial");
            var polSub= Polynomial.Subtraction(p1, p2);
            Console.WriteLine(polSub);
            Console.WriteLine();

            Console.WriteLine("Multiplication two polynomial");
            var polMul = Polynomial.Multiplication(p1, p2);
            Console.WriteLine(polMul);
            Console.WriteLine();

            Console.ReadKey();
        }
    }
}
