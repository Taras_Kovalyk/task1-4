﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplicationTask1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Create first vector of a given size 5");
            Vector vector1 = new Vector(5);

            Console.WriteLine("Create second vector of a given size 5");
            Vector vector2 = new Vector(5);

            Console.WriteLine("Сreate a vector3 with the given boundaries 0-4");
            Vector vector3 = new Vector(0, 4);

            Console.WriteLine("Fill an array with values from 1 to 5");
            for (int i = 0; i < vector1.Count; i++)
            {
                vector1[i] = i + 1;
                vector2[i] = i + 1;
            }
            Console.WriteLine("Print the first array");
            PrintVector(vector1);

            Console.WriteLine("\nAdding arrays vector1+vector2");

            PrintVector(Vector.SumArray(vector1, vector2));

            Console.WriteLine("Fill vector3 with values from 5 to 9");
            for (int i = 0; i < vector1.Count; i++)
            {
                vector3[i] = i + 5;
            }
            Console.WriteLine("Print the vector3");
            PrintVector(vector3);

            Console.WriteLine("Subtract vector 3 from vector 1");

            PrintVector(Vector.SubtractArray(vector1, vector3));

            Console.WriteLine("Print vector1");
            PrintVector(vector1);
            Console.WriteLine("Print vector2");
            PrintVector(vector2);
            Console.WriteLine("Print vector3");
            PrintVector(vector3);

            Console.WriteLine("comparing vectors : vector1 and vector2");
            Console.WriteLine(vector1.Equals(vector2));
            Console.WriteLine("comparing vectors : vector1 and vector3");
            Console.WriteLine(vector1.Equals(vector3));

            Console.WriteLine("Try entering an incorrect index 435");
            try
            {
                vector1[435] = 45;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.ReadKey();
        }

        private static void PrintVector(Vector vector1)
        {
            foreach (var item in vector1)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();
        }
    }
}




